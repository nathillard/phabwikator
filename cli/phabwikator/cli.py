__version__ = '0.1.0'

import pprint

import typer
from phabricator import Phabricator

from .config import settings
from .models import PhabSprint, QueryType

app = typer.Typer()

def shared_init(project_name:str) -> PhabSprint:
    phab = Phabricator(host='https://phabricator.wikimedia.org/api/', token=settings.conduit_token)
    phab.update_interfaces()
    return PhabSprint.from_project_name(phab, project_name)

@app.command()
def columns(name:str, query_type:QueryType = QueryType.OPEN_ONLY):
    typer.echo("Initializing sprint")
    s = shared_init(name)
    typer.echo("Fetching columns (this may take a while)...")
    t_by_c = s.tasks_by_column(query_type=query_type)
    pprint.pprint(dict(t_by_c))

@app.command()
def issues(name: str, query_type:QueryType = QueryType.OPEN_ONLY):
    typer.echo("Initializing sprint")
    s = shared_init(name)
    typer.echo(f"Printing issues for project {name} with query type {query_type}")
    print("Task ID || Task Name || Date Modified")
    for task in s.tasks(query_type):
        print(f"T{task.id}", "||", task.name, "||",  task.date_modified)

def main():
    app()

if __name__ == "__main__":
    main()
