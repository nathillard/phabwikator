import re
from collections import OrderedDict, defaultdict
from datetime import datetime, timedelta
from enum import Enum
from subprocess import PIPE, STDOUT, Popen
from typing import Dict, List, Optional
import base64

from phabricator import Phabricator
import dataclasses
import numpy as np

import logging
logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)
logger.setLevel("INFO")

class PhabObject:
    def __init__(self, ref:Dict, phab:Phabricator, hydrated:bool = True) -> None:
        self.phab = phab
        self._ref = ref
        self.hydrated = hydrated
        
    def __getattr__(self, prop):
        if prop in ["id", "type", "phid", "attachments"]:
            return self._ref.get(prop)
        elif self._ref.get("fields"):
            return self._ref.get("fields").get(prop)
        else:
            return self._ref.get(prop)
    
    @classmethod 
    def from_query(cls, phab:Phabricator, query:Dict):
        constraints = {
            "query": query
        }
        ref = cls.search(phab, constraints)
        return cls(phab=phab, ref=ref)

    @classmethod 
    def from_phid(cls, phab:Phabricator, phid:str):
        constraints = {
            "phids": [phid]
        }
        ref = cls.search(phab, constraints)
        if len(ref):
            return cls(phab=phab, ref=ref)
        else:
            logger.warning(f"'{phid}' not found")
            return None
    
    @classmethod 
    def from_task_id(cls, phab:Phabricator, task_id:str):
        constraints = {
            "ids": [int(task_id[1:])]
        }
        ref = cls.search(phab, constraints)
        return cls(phab=phab, ref=ref)
    
    @classmethod 
    def from_dict(cls, phab:Phabricator, d:Dict):
        return cls(phab=phab, ref=d)
    
    @classmethod 
    def from_basic_dict(cls, phab:Phabricator, d:Dict):
        return cls(phab=phab, ref=d, hydrated=False)
    
    def hydrate(self):
        ref = self.search(self.phab, constraints={"phids": [self.phid]})
        self._ref = ref
        self.hydrated = True
    
    def __repr__(self):
        return f'{self.__class__.__name__}("{self.id}", "{self.phid}", "{self.name}")'
    
    def __eq__(self, obj):
        return self._ref == obj._ref
    
    def __hash__(self):
        return hash(self._ref)


class PhabProject(PhabObject):
    @classmethod
    def search(cls, phab, constraints:Dict):
        return phab.project.search(constraints=constraints).data[0]

    @classmethod 
    def from_project_name(cls, phab:Phabricator, project_name:str):
        return PhabProject.from_query(phab, f"title:'{project_name}'" )
    
    @property
    def columns(self):
        if not self._columns:
            self._columns = []
            cols = self.phab.project.column.search(constraints={'projects': [self.phid]}, attachments={"columns" : True}).data
            for col in cols:
                self._columns.append(PhabColumn.from_dict(self.phab, col))
            self._columns = sorted(self._columns, key=lambda c: c.sequence)
        return self._columns

    def __repr__(self):
        return f'{self.__class__.__name__}("{self.phid}", {self.name})'

class QueryType(str, Enum):
    OPEN_ONLY = "open"
    CLOSED_ONLY = "closed"
    ALL = "all"

class PhabSprint(PhabObject):
    def __init__(self, project:PhabProject, *args, **kwargs) -> None:
        self.project = project
        self._tasks = None
        self._tasks_by_column = None
        self._tasks_by_status = None
        self._possible_statuses = None
        # self.ordered_columns = ["Committed", "Research", "Ready for Design", "In Design", "Design Review", "Ready for Development", "In Development", "Code Review", "Design Sign-Off", "Functional Testing", "Product Sign-Off", "Pending Release"]
        self.lower_date_limit = None
        self.upper_date_limit = None
        self.query_type = QueryType.ALL
        super().__init__(*args, **kwargs)
    
    def generate_gantt_chart(self, simple:bool = True) -> str:
        chart = "gantt\n"
        chart += f"    title Phabricator Task Timeline\n"
        chart += f"    dateFormat  YYYY-MM-DD\n"
        for task in self.tasks:
            chart += task.generate_gantt_section_info(simple=simple)
        return chart


    @classmethod 
    def from_project_name(cls, phab:Phabricator, project_name:str):
        p = PhabProject.from_query(phab, f"title:'{project_name}'" )
        constraints = {
            "projects": [p.phid]
        }
        ref = cls.search(phab, constraints)
        return cls(project=p, phab=phab, ref=ref)

    @classmethod
    def search(cls, phab, constraints:Dict):
        return phab.maniphest.search(constraints=constraints, attachments={"projects": True, "columns" : True}).data
    
    @property
    def possible_statuses(self):
        if not self._possible_statuses:
            self._possible_statuses = self.phab.maniphest.status.search().data
        return self._possible_statuses

    @property
    def closed_statuses(self):
        return [s["name"] for s in self.possible_statuses if s["closed"]]
    
    @property
    def open_statuses(self):
        return [s["name"] for s in self.possible_statuses if not s["closed"]]

    @property
    def tasks(self):
        if not self._tasks:
            self._tasks = []
            for task in self._ref:
                task = PhabTask.from_dict(self.phab, task)
                task.sprint = self
                self._tasks.append(task)
        task_filter = None
        if self.query_type == QueryType.OPEN_ONLY:
            task_filter = lambda t: t.status["name"] in self.open_statuses
        elif self.query_type == QueryType.CLOSED_ONLY:
            task_filter = lambda t: t.status["name"] in self.closed_statuses
        return list(filter(task_filter, self._tasks))

    @property
    def parent_tasks(self):
        t_by_p = self.tasks_by_parents
        sorted_parents = {k: v for k, v in sorted(t_by_p.items(), key=lambda item: len(item[1]), reverse=True)}
        for task_id, children in sorted_parents.items():
            if task_id == "None":
                logger.debug(task_id)
            else:
                task = PhabTask.from_task_id(self.phab, f"T{task_id}")
                logger.debug(task.id, task.name, ":")
            for child in children:
                logger.debug("\t", child.id, child.name)
            logger.debug("\n\n")
    
    @property
    def tasks_by_status(self):
        return {
            "open" : self.tasks(query_type=QueryType.OPEN_ONLY),
            "closed": self.tasks(query_type=QueryType.CLOSED_ONLY)
        }
    
    @property
    def tasks_by_parents(self):
        parents = defaultdict(list)
        for t in self.tasks(self.query_type):
            if len(t.parents):
                parents[t.parents[0].id].append(t)
            else:
                parents["None"].append(t)
        return parents

    def tasks_for_column_name(self, column_name:str):
        return [t for t in self.tasks if t.columns_for_project_name(self.project.name)[0].name == column_name]
    
    @property
    def tasks_by_column(self):
        tasks_by_column = defaultdict(list)
        target_task_list = self.tasks
        for task in target_task_list:
            for col in self.project.columns:
                if task.columns_for_project_name(self.project.name)[0].name == col.name:
                    tasks_by_column[col.name].append(task)
        return tasks_by_column
    
    @property
    def column_stats(self):
        self.ordered_column_stats = OrderedDict()
        for column in self.ordered_columns:
            self.ordered_column_stats[column] = self.tasks_by_column[column]
        return self.ordered_column_stats

    def __getattr__(self, prop):
        #return super().__getattr__(prop)
        return object.__getattribute__(self, prop)
    
    # @property
    # def basic_stats(self):
    #     info = {
    #         "Number of Tasks": self.num_tasks,
    #         "Total Points": self.total_points,

    #     }

    #     newly_added = []
    #     for t in self.tasks:
    #         how_long_ago_created = datetime.today() - t.date_created
    #         if how_long_ago_created.days < self.lower_date_limit:
    #             newly_added.append(t)

    #     resolved_this_sprint = []
    #     for t in self.tasks_by_status["closed"]:
    #         how_long_ago_resolved = datetime.today() - t.date_modified
    #         if how_long_ago_resolved.days < self.lower_date_limit:
    #             resolved_this_sprint.append(t)
    #     logger.info(f"{len(resolved_this_sprint)} tickets resolved in the last {self.lower_date_limit} days: {resolved_this_sprint}")
    #     logger.info(f"{len(newly_added)} tickets added in the last {self.lower_date_limit} days: {newly_added}")
    
    @property
    def interrupt_work(self) -> list("PhabTask"):
        new_work = []
        for task in self.tasks:
            if task.first_moved_into_sprint_new(self.project.phid) > self.lower_date_limit:
                new_work.append(task)
        return new_work
    
    @property
    def interrupt_point_total(self):
        return sum(int(t.points) for t in self.interrupt_work if t.points)
    
    @property
    def num_tasks(self):
        return len(self.tasks)

    @property
    def total_points(self):
        return sum(int(t.points) for t in self.tasks if t.points)
    
    def __repr__(self):
        return f'{self.__class__.__name__}(project="{self.project}")'

class PhabTask(PhabObject):
    def __init__(self, phab:Phabricator, ref:Dict) -> None:
        self.phab = phab
        self._ref = ref
        self._transaction_re = re.compile(r"""
            (?P<user>.*?)
            \ moved\ this\ task
            (?:
                \ from\ 
                (?P<before>.*?)
            )?
            (?:
                \ to\ 
                (?P<after>.*?)
            )?
            \ on\ 
            (
                (
                    the\ 
                    (?P<board>.*?)
                    \ board
                )
                |
                (
                    (?P<num_boards>[0-9]+)
                    \ board\(s\):\ 
                    (?P<board_list>.*?)
                )
            )
            \.""", re.VERBOSE)
        self.sprint = None

    def parse_transaction(self, trans_title):
        match = self._transaction_re.match(trans_title)
        if match:
            return match.groupdict()
        else:
            return trans_title

    @classmethod
    def search(cls, phab, constraints:Dict):
        return phab.maniphest.search(constraints=constraints, attachments={"projects": True, "columns" : True}).data[0]

    @property
    def age_str(self):
        return str(datetime.now() - datetime.fromtimestamp(self._ref["fields"]["dateCreated"]))
    
    @property
    def age(self):
        return datetime.now() - datetime.fromtimestamp(self._ref["fields"]["dateCreated"])
    
    @property
    def date_created(self):
        return datetime.fromtimestamp(self.dateCreated)
    
    @property
    def date_modified(self):
        return datetime.fromtimestamp(self.dateModified)
    
    @property
    def date_closed(self):
        return datetime.fromtimestamp(self.dateClosed)

    @property
    def author(self):
        if not self._author and self.authorPHID:
            self._author = PhabUser(phab=self.phab, ref=self.phab.user.search(constraints={"phids": [self.authorPHID]}).data[0])
        return self._author
    
    @property
    def owner(self):
        if not self._owner and self.ownerPHID:
            self._owner = PhabUser(phab=self.phab, ref=self.phab.user.search(constraints={"phids": [self.ownerPHID]}).data[0])
        return self._owner
    
    @property
    def tags(self):
        if not self._tags:
            self._tags = []
            for phid in self.attachments["projects"]["projectPHIDs"]:
                self._tags.append(PhabProject.from_phid(self.phab, phid))
        return self._tags
    
    @property
    def columns(self):
        if not self._columns:
            self._columns = []
            for info in self.attachments["columns"]["boards"].values():
                for col in info.get("columns"):
                    basic_col = PhabColumn.from_basic_dict(self.phab, col)
                    basic_col.hydrate()
                    self._columns.append(basic_col)
        return self._columns
    
    def columns_for_project_name(self, project_name:str):
        return list(filter(lambda c: c.project["name"] == project_name, self.columns))
    
    @property
    def transactions(self):
        return self.transactions_old
    
    @property
    def transactions_old_raw(self):
        if not self._transactions_old_raw:
            self._transactions_old_raw = list(self.phab.maniphest.gettasktransactions(ids=[self.id]).values())[0]
        return self._transactions_old_raw

    @property
    def column_transactions(self):
        return filter(lambda tr: type(tr) == PhabTransactionColumnMove, self.transactions_old)

    @property
    def transactions_old(self):
        if not self._transactions:
            transactions_raw = list(self.phab.maniphest.gettasktransactions(ids=[self.id]).values())[0]
            relevant_transactions_raw = transactions_raw
            if self.sprint.lower_date_limit and self.sprint.upper_date_limit:
                relevant_transactions_raw = (t for t in transactions_raw if float(t["dateCreated"]) > self.sprint.lower_date_limit.timestamp() and float(t["dateCreated"]) < self.sprint.upper_date_limit.timestamp())
            self._transactions = [PhabTransactionOld.from_dict(phab=self.phab, input_dict=tr) for tr in relevant_transactions_raw]
            self._transactions = list(reversed(self._transactions))
        return self._transactions

    @property
    def started_dev_transaction(self):
        relevant_transactions = list(filter(lambda tr: tr and tr.to_col.name == "Doing" and tr.from_col != tr.to_col, self.transactions_old))
        if len(relevant_transactions) == 1:
            return relevant_transactions[0]
        else:
            logger.info(f"Something odd is happening with ticket {self.id}...")
            return None

    @property
    def finished_dev_transaction(self):
        return list(filter(lambda tr: tr and tr.to_col.name == "Ready for Signoff" and tr.from_col != tr.to_col, self.transactions_old))[0]
    
    def transaction_for_shift(self, to_col: str, from_col: Optional[str] = None) -> Optional["PhabTransactionColumnMove"]:
        for tr in self.column_transactions:
            if type(tr) == PhabTransactionColumnMove and tr.to_col.name == to_col and (not from_col or tr.from_col.name == from_col):
                if self.sprint.lower_date_limit and tr.date_created < self.sprint.lower_date_limit:
                    continue
                if self.sprint.upper_date_limit and tr.date_created > self.sprint.upper_date_limit:
                    continue
                return tr
        return None
    
    @property
    def time_in_progress(self):
        if not self._time_in_progress:
            started_dev = self.transaction_for_shift(to_col="Doing")
            if started_dev:
                logger.debug(f"Ticket {self.id} was indeed moved to 'Doing'")
            else:
                logger.debug(f"Ticket {self.id} was never moved to 'Doing', checking Code Review")
                started_dev = self.transaction_for_shift(to_col="Code Review")
                if started_dev:
                    logger.debug(f"Ticket {self.id} was indeed moved to 'Code Review'")
                else:
                    logger.debug(f"Ticket {self.id} was never moved to 'Code Review'")

            finished_dev = self.transaction_for_shift(to_col="QA")
            if finished_dev:
                logger.debug(f"Ticket {self.id} was indeed moved to 'QA'")
            else:
                logger.debug(f"Ticket {self.id} was never moved to 'QA', checking Ready for Signoff")
                finished_dev = self.transaction_for_shift(to_col="Ready for Signoff")
                if finished_dev:
                    logger.debug(f"Ticket {self.id} was indeed moved to 'Ready for Signoff'")
                else:
                    logger.debug(f"Ticket {self.id} was never moved to 'Ready for Signoff'")

            if started_dev and finished_dev:
                logger.debug(f"Started: {started_dev.date_created}, Finished: {finished_dev.date_created}")
                self._time_in_progress = np.busday_count(started_dev.date_created.date(), finished_dev.date_created.date())
            else: 
                logger.debug(f"Ticket {self.id} was either not started or finished in this sprint")
                self._time_in_progress = 0
        return self._time_in_progress

    @property 
    def time_to_qa(self):
        return list(filter(lambda tr: tr and tr.to_col.name == "QA" and tr.from_col != tr.to_col, self.transactions_old))[0]

    @property
    def transactions_new_raw(self):
        return self.phab.transaction.search(objectIdentifier=f'T{self.id}').data

    @property
    def transactions_new(self):
        if not self._transactions_new:
            transaction_dict_list = self.phab.transaction.search(objectIdentifier=f'T{self.id}').data
            self._transactions_new = [PhabTransaction(phab=self.phab, ref=td, **td) for td in transaction_dict_list]
        return self._transactions_new

    def relevant_transactions(self, start_date, end_date, hydrate: bool = False):
        return [t for t in self.transactions_old_raw if float(t["dateCreated"]) > start_date.timestamp() and float(t["dateCreated"]) < end_date.timestamp()]

    def first_moved_into_sprint_new(self, project_phid):
        found_transaction = None
        for transaction in self.transactions_new_raw:
            if transaction["type"] == "projects":
                for operation in transaction["fields"]["operations"]:
                    if operation["operation"] == "add" and operation["phid"] == project_phid:
                        found_transaction = transaction
            if found_transaction:
                break
        # logger.debug(found_transaction)
        # transaction_obj = PhabTransactionOld.from_dict(input_dict=found_transaction, phab=self.phab)
        date_obj = datetime.fromtimestamp(int(found_transaction["dateCreated"]))
        return date_obj
    
    @property
    def first_moved_into_sprint(self):
        found_transaction = None
        for transaction in self.transactions_old:
            if transaction and transaction.to_col.name == "Incoming":
                found_transaction = transaction
                break
        if not found_transaction:
            logger.debug(f"issue {self.id} has no relevant transactions")
        return found_transaction 

    @property
    def comments(self):
        return [t for t in self.transactions if t["transactionType"] == "core:comment"]

    @property
    def parents(self) -> List["PhabTask"]:
        if not self._parents:
            self._parents = []
            parents_data = self.phab.edge.search(sourcePHIDs=[self.phid], types=['task.parent']).data
            if len(parents_data):
                for parent_data in parents_data:
                    parent_phid = parent_data["destinationPHID"]
                    if parent_phid:
                        parent = PhabTask.from_phid(self.phab, parent_phid)
                        self._parents.append(parent)
        return self._parents

    def generate_gantt_section_info(self, simple:bool = True) -> str:
        if not self.transactions_old:
            return None
        chart = f"\n    section T{self.id}\n"
        if simple:
            first_transition = self.transactions_old[0]
            first_transition_start = datetime.strftime(first_transition.date_created, "%Y-%m-%d")

            last_transition = self.transactions_old[-1]
            last_transition_start = datetime.strftime(last_transition.date_created, "%Y-%m-%d")

            chart += f"    {first_transition.to_col.name} : {first_transition_start}\n"
            chart += f"    {last_transition.to_col.name} : {last_transition_start}\n"
        else:
            prev_trans = None
            for i, trans in enumerate(self.transactions_old):
                if trans:
                    task_start = datetime.strftime(trans.date_created, "%Y-%m-%d")
                    if prev_trans is not None and trans.from_col == prev_trans.to_col:
                        duration = np.busday_count(prev_trans.date_created.date(), trans.date_created.date())
                        chart += f"    {trans.to_col.name} : {trans.date_created:%Y-%m-%d}, {duration}d\n"
                    else:
                        chart += f"    {trans.to_col.name} : {task_start}\n"
                    prev_trans = trans
        return chart
    
    def generate_gantt_chart(self, simple:bool = True) -> str:
        chart = "gantt\n"
        chart += f"    title Phabricator Task Timeline\n"
        chart += f"    dateFormat  YYYY-MM-DD\n"
        chart += self.generate_gantt_section_info(simple=simple)
        return chart

    @property
    def sequence_diagram(self):
        rows = []
        rows.append("sequenceDiagram")
        # columns = ["Committed", "Research", "Ready for Design", "In Design", "Design Review", "Ready for Development", "In Development", "Code Review", "Design Sign-Off", "Functional Testing", "Product Sign-Off", "Pending Release"]
        columns = [c.name for c in self.sprint.project.columns]
        for column in columns:
            column = column.replace("-", " ")
            rows.append(f"\tparticipant {column}")
        step_num = 1
        for t in reversed(self.transactions_old_raw):
            if t["transactionType"] == "core:columns" and t["newValue"][0]["boardPHID"] == self.sprint.project.phid:
                parsed = self.parse_transaction(t["title"])
                before = parsed.get("before")
                after = parsed.get("after")
                if before in columns and after in columns:
                    before = before.replace("-", " ")
                    after = after.replace("-", " ")
                    change = f"{before}->>+{after}: {step_num}" #
                    rows.append(f"\t{change}")
                    step_num += 1
        return '\n'.join(rows)
    
    def generate_diagram(self):
        p = Popen(['npx', '-p', '@mermaid-js/mermaid-cli', 'mmdc'], stdout=PIPE, stdin=PIPE, stderr=PIPE)
        stdout_data = p.communicate(input=bytes(self.history_graph, "utf-8"))[0]
        return stdout_data


class PhabUser(PhabObject):
    @classmethod
    def search(cls, phab, constraints:Dict):
        return phab.user.search(constraints=constraints).data[0]

    def __repr__(self):
        return f'{self.__class__.__name__}("{self.phid}", {self.realName})'

class PhabColumn(PhabObject):
    @classmethod
    def search(cls, phab, constraints:Dict):
        return phab.project.column.search(constraints=constraints).data[0]
    
    @property
    def columns(self):
        if not self._columns:
            self._columns = self.phab.project.column.search(constraints={'projects': [self.phid]}, attachments={"columns" : True})
        return self._columns
    
    @property
    def project_obj(self):
        if not self._project_obj:
            self._project_obj = PhabProject.from_phid(self.phab, self.project["phid"])
        return self._project_obj

    def __repr__(self):
        return f'{self.__class__.__name__}("{self.phid}", {self.name})'


@dataclasses.dataclass
class PhabColumnDC:
    pass

@dataclasses.dataclass
class PhabPHIDQueryResponse:
    phid: str
    uri: str
    typeName: str
    type: str
    name: str
    fullName: str
    status: str
    ref: dict = dataclasses.field(repr=False)

    @classmethod
    def from_phid(cls,phab, phid):
        query_resp_dict = phab.phid.query(phids = [phid])[phid]
        # logger.debug(query_resp_dict)
        return cls(ref=query_resp_dict, **query_resp_dict)

@dataclasses.dataclass
class PhabOperation:
    operation: str
    phid: str
    phab: Phabricator
    ref: dict = dataclasses.field(repr=False)
    info: PhabPHIDQueryResponse | None = None
    project: str | None = None
    
    def __post_init__(self):
        query_dict = self.phab.phid.query(phids = [self.phid])[self.phid]
        self.info = PhabPHIDQueryResponse(ref=query_dict, **query_dict)
        if self.info.type == "PROJECT":
            self.project = PhabProject.from_phid(self.phab, self.phid)
      
      
class OldTransactionType(str, Enum):
    COLUMNS = "core:columns"
    CREATE = "core:create"
    COMMENT = "core:comment"
    DESCRIPTION = "description"
    EDGE = "core:edge"
    EDIT_POLICY = "core:edit-policy"
    SUBSCRIBERS = "core:subscribers"
    VIEW_POLICY = "core:view-policy"
    POINTS = "points"
    PRIORITY = "priority"
    REASSIGN = "reassign"
    TITLE = "title"

@dataclasses.dataclass
class PhabAuthor:
    id: int
    type: str
    phid: str
    fields: dict
    attachments: dict
    ref: dict = dataclasses.field(repr=False)

    def __post_init__(self):
        for field, value in self.fields.items():
            self.__setattr__(field, value)

    @classmethod
    def from_phid(cls,phab,phid):
        query_resp_dict = phab.user.search(constraints={"phids": [phid]})["data"][0]
        # logger.debug(query_resp_dict)
        return cls(ref=query_resp_dict, **query_resp_dict)

@dataclasses.dataclass
class PhabTransactionOld:
    taskID: str
    title: str
    transactionID: str
    transactionPHID: str
    transactionType: str
    oldValue: any
    newValue: any
    meta: dict
    comments: str
    authorPHID: str
    dateCreated: str
    phab: Phabricator
    ref: dict = dataclasses.field(repr=False)

    author: str | None = None
    date_created: datetime | None = None

    @classmethod
    def from_dict(cls, input_dict: dict, phab: Phabricator) -> "PhabTransactionOld":
        transactionType = input_dict.get("transactionType")
        match transactionType:
            case OldTransactionType.COLUMNS :
                return PhabTransactionColumnMove.from_dict(input_dict, phab)
            # case OldTransactionType.CREATE :
            # case OldTransactionType.COMMENT :
            # case OldTransactionType.DESCRIPTION :
            # case OldTransactionType.EDGE :
            # case OldTransactionType.EDIT_POLICY :
            # case OldTransactionType.SUBSCRIBERS :
            # case OldTransactionType.VIEW_POLICY :
            # case OldTransactionType.POINTS :
            # case OldTransactionType.PRIORITY :
            # case OldTransactionType.REASSIGN :
            # case OldTransactionType.TITLE : 
            case _:
                return cls(**input_dict, ref=input_dict, phab=phab)

    def __post_init__(self):
        if self.dateCreated:
            self.date_created = datetime.fromtimestamp(int(self.dateCreated))
        self.author = PhabAuthor.from_phid(self.phab, self.authorPHID)
        

@dataclasses.dataclass
class PhabTransactionColumnMove(PhabTransactionOld):
    from_col: PhabColumn | None = None
    to_col: PhabColumn | None = None 

    @classmethod
    def from_dict(cls, input_dict: dict, phab: Phabricator) -> "PhabTransactionOld":
        return cls(**input_dict, ref=input_dict, phab=phab)

    def __post_init__(self):
        super().__post_init__()
        new_val = self.newValue[0]

        from_col_PHIDs_raw = new_val.get('fromColumnPHIDs')
        if from_col_PHIDs_raw:
            from_col_phid = list(from_col_PHIDs_raw.values())[0]
            self.from_col = PhabColumn.from_phid(phab=self.phab, phid=from_col_phid)

        to_col_phid = new_val.get('columnPHID')
        if to_col_phid:
            self.to_col = PhabColumn.from_phid(phab=self.phab, phid=to_col_phid)
        
@dataclasses.dataclass
class PhabTransaction:
    id: int
    phid: str
    type: str
    authorPHID: str
    objectPHID: str
    dateCreated: int
    dateModified: int
    groupID: str
    comments: list[str]
    fields: dict[str, str]
    phab: Phabricator
    ref: dict = dataclasses.field(repr=False)

    author: str | None = None
    date_modified: datetime | None = None
    date_created: datetime | None = None
    operations: list[PhabOperation] | None = None

    def __post_init__(self):
        if self.dateCreated:
            self.date_created = datetime.fromtimestamp(self.dateCreated)
        if self.dateModified:
            self.date_modified = datetime.fromtimestamp(self.dateModified)
        ops_dict = self.fields.get("operations")
        if ops_dict:
            self.operations = [PhabOperation(phab=self.phab, ref=o, **o) for o in ops_dict]
        if self.authorPHID:
            author_search_results = self.phab.user.search(constraints={"phids": [self.authorPHID]}).data
            if len(author_search_results) == 1:
                self.author = PhabUser(phab=self.phab, ref=author_search_results[0])
            else:
                logger.warn(f"Couldn't find author for PHID {self.authorPHID}")
        self.process_transaction()
    
    def process_transaction(self):
        if self.type == "projects":
            for operation in self.operations:
                operation.project = PhabProject.from_phid(self.phab, operation.phid)
    


