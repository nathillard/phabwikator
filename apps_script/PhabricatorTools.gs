var CONDUIT_TOKEN = "YOUR_CONDUIT_TOKEN_HERE";
var PHAB_DOMAIN = "phabricator.wikimedia.org";
var USER_AGENT = 'Google Sheets ( Phabwikator )';
var PROJECT_PHID = "PHID-PROJ-7x2jagmev5l5dyco323o";

// Methods for use in cells!
function phabTitle(taskId){return getTaskData(taskId).title ? getTaskData(taskId).title : "N/A"}
function phabStatus(taskId){return getTaskData(taskId).status}
function phabAuthor(taskId){return getTaskData(taskId).author}
function phabDateCreated(taskId){return getTaskData(taskId).created}
function phabPoints(taskId){return getTaskData(taskId).points}
function phabTags(taskId){return getTaskData(taskId).tags}

function phabBoardColumn(taskId,boardShortcut){return getTaskData(taskId).boards[boardShortcut]}

function epochToDateString( timestamp ) {
  let date = new Date(parseInt(timestamp) * 1000);
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  return year + "-" + month + "-" + day;
}

function getCachedOrCreate(key, fn) {
  let cache = CacheService.getDocumentCache();
  let result = null;

  const cached = cache.get( key );
  if(cached && !cached.search("Error")) {
    result = cached;
  } else {
    result = fn();
    if (result.data) {
      cache.put( key, result, 60 * ( 30 + Math.floor(Math.random() * 60) ) );  
    }
  }
  return result;
}

function searchWithPayload(endpointName, customPayload) {
  const payload = {
        'method' : 'post',
        'payload' : {...customPayload,
          'api.token': CONDUIT_TOKEN
        },
        'headers' : {
          'User-Agent': USER_AGENT
        }
  };
  function doSearch() {
    const response = UrlFetchApp.fetch(`https://${PHAB_DOMAIN}/api/${endpointName}`,payload);
    const responseText = response.getContentText();
    return responseText;
  }
  const cachedOrCreated = getCachedOrCreate(payload, doSearch);
  const cachedOrCreatedObject = JSON.parse(cachedOrCreated);
  return cachedOrCreatedObject;
}

/**
 * @let string userPHID Example: "PHID-USER-u7w6n5ecde66oujx33pe"
 */
function getUserData(userPHID) {
  const userPayload = {
    'constraints[phids][0]': userPHID
  };
  const userData = searchWithPayload( "user.search", userPayload ).result.data[0];
  return {
    'id': userData.id,
    'name': userData.fields.username,
  }
}

/**
 * @let string taskId Example: "T1234"
 */
function getParentTaskData(taskID) {
  const taskPHID = getTaskData(taskID).phid;
  const edgePayload = {
          'sourcePHIDs[0]' : taskPHID,
          'types[0]': 'task.parent'
  };
  const edgeResult = searchWithPayload( "edge.search", edgePayload );
  if (edgeResult.error_code) {
    return `ERROR ${edgeResult.error_code}`;
  } else if (edgeResult.result.data.length) {
    const parentPHID = edgeResult.result.data[0].destinationPHID;
    const parentPayload = {"constraints[phids][0]": parentPHID};
    const parentID = searchWithPayload( "maniphest.search", parentPayload).result.data[0].id;
    return "T" + parentID;
  } else {
    return "N/A"
  }
}

function getTagName(tagPHID) {
  const tagPayload = {
    "constraints[phids][0]": tagPHID
  };
  const searchResult = searchWithPayload( "project.search", tagPayload).result.data[0].fields.name;
  return searchResult;
}

/**
 * @let string taskId Example: "T1234"
 */
function getTaskData(taskId) {
  const taskPayload = {
      'constraints[ids][0]': taskId.substring(1),
      'attachments[columns][boards][columns]': true, 
      'attachments[projects]': true
  };
  const rawData = searchWithPayload( "maniphest.search", taskPayload);
  
  let taskData = null;
  if (rawData.result) {
    taskData = rawData.result.data[0];
  } else {
    return rawData;
  }
  return getDataDictionary(taskData);
}

function getDataDictionary(rawData) {  
  let column = '';
  if (rawData.attachments.columns.boards[PROJECT_PHID]) {
    column = rawData.attachments.columns.boards[PROJECT_PHID].columns[0].name;
  }
  
  let ownerName;
  if (rawData.fields.ownerPHID) {
    ownerName = getUserData(rawData.fields.ownerPHID).name;
  } else {
    ownerName = "N/A";
  }

  let authorName;
  if (rawData.fields.authorPHID) {
    authorName = getUserData(rawData.fields.authorPHID).name;
  } else {
    authorName = "N/A";
  }

  let tagList = rawData.attachments.projects.projectPHIDs.map(getTagName).join(",");

  return {
    'id': rawData.id,
    'phid': rawData.phid,
    'title': rawData.fields.name,
    'status': rawData.fields.status.name,
    'priority': rawData.fields.priority.value,
    'author': authorName,
    'owner': ownerName,
    'created': epochToDateString(rawData.fields.dateCreated),
    //'points': rawData.fields.points,
    'column': column,
    'tags': tagList
  }
}

function getAllSprintTasks() {
  const sprintPayload = {
     "constraints[projects][0]": PROJECT_PHID,
     "attachments[columns][boards][columns]": true,
     "attachments[projects]": true,
     "queryKey": "open"
  };
  const rawData = searchWithPayload( "maniphest.search", sprintPayload);
  if (rawData.result) {
    const sprintData = rawData.result.data;
    const data = sprintData.map(task => Object.values(getDataDictionary(task)));
    return data;
  } else {
    return rawData.error_info;
  }
}

