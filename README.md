# phabwikator

## Name
Phabwikator

## Description
An interface into the [Phabricator](https://phabricator.wikimedia.org/) issue-tracking system for use in [Wikimedia](https://www.wikimedia.org/) projects. 

There are two parts:
1. CLI - a command-line utility for interfacing with phabricator tickets
2. App Script - A [Google Apps Script](https://developers.google.com/apps-script) library for working with Phabricator data in spreadsheets

## Installation
### CLI
The CLI project is built in Python, and uses [poetry](https://python-poetry.org/docs/) for package management. 

To install:
1. Install Poetry as described [here](https://python-poetry.org/docs/)
2. 
```bash
poetry install
```
This will create a virtual environment, and install the packages referenced in [pyproject.toml](./cli/pyproject.toml) into it. See below under [Usage](###Usage) for how to use Poetry during development. 

### Apps Script
The Apps Script project is for use in Google Apps apps scripts, particularly for Google Sheets deployments. 

#### Copy and Paste
The easiest way to use this script at present is the following:
1. go to http://www.sheets.new to create a new Google Sheet
2. Go to Extensions -> App Script to open the script editor for a sheet in question, and paste in the code from [PhabricatorTools.gs](./apps_script/PhabricatorTools.gs).
3. Change the follwoing variables in the script:
a. Change `PROJECT_PHID` to your project's PHID. To find this, go to "https://phabricator.wikimedia.org/conduit/method/project.search/" and add `{"name": "PROJECT_BOARD"}` into the `constraints` field where `PROJECT_BOARD` is your board's name. Hit "Call Method". In the result, check the entry for `phid`
b. Change `CONDUIT_TOKEN` to your Phabricator Conduit API token. See below for how to do this.

#### Clasp API
Eventually, you will be able to use the [clasp](https://developers.google.com/apps-script/guides/clasp) tool to deploy to your own environment. See instructions on the [clasp page](https://developers.google.com/apps-script/guides/clasp) for doing this, but in short:

Then go to https://script.google.com/home/usersettings and enalbe "Google Apps Script API". 

Finally, run the following:
```bash
npx install @google/clasp
npx @google/clasp login
npx @google/clasp push
```

## Usage
### Prerequisites
To run either tool, it's necessary to first get an API token. To do so:
1. Go to https://phabricator.wikimedia.org 
2. Click on your profile image dropdown in the upper right and select `settings`
3. In the menu on the left-hand side, select `Conduit API Tokens` 
4. Hit `+ Generate Token` and take down the token. 
5. Save the API token into the `CONDUIT_TOKEN` environment variable 

### CLI
To run this tool, it's necessary to first get an API token. To do so:
1. Go to https://phabricator.wikimedia.org 
2. Click on your profile image dropdown in the upper right and select `settings`
3. In the menu on the left-hand side, select `Conduit API Tokens` 
4. Hit `+ Generate Token` and take down the token. 
5. Save the API token into the `CONDUIT_TOKEN` environment variable 

This tool uses [Pydantic](https://pydantic-docs.helpmanual.io/) for storing settings, so you can either create a custom `settings.settings` object or export an environment variable named the same as the variable but in caps, as in `CONDUIT_TOKEN`, which should then be captured by your script. 

It also uses [Typer](https://typer.tiangolo.com/) to create a simple cli. 

To run it, you can do :
```bash
poetry run cli --help
```
to get info on commands to run. See [cli.py](./cli/phabwikator/cli.py) for more info on what can be run.

Some example queries:
1. Get all open issues on a given project board.  
```bash
poetry run cli issues --query-type open "Design-Systems-Sprint"
```

2. Get all columns for a given project board
Run 
```bash
poetry run cli columns --query-type open "Design-Systems-Sprint"
```

### Apps Script
After running through the above setup, you can enter the function names in individual cells to get the data of interest, as in `=phabTitle("T12345")`, and it will populate that cell with relevant info. See the top of the script for the available funcitons to run. 

## Support
Until we have an official slack channel, please reach out directly to Nat Hillard

## Roadmap
TBD!

## Contributing
This is still an early-stage project, and use cases mostly work for a single project at the moment. Please reach out with questions and concerns and interest in contributing, but we may need to lay a bit of groundwork first before this is easier. 

## Authors and acknowledgment
* Thanks to Addshore for your [blog post](https://addshore.com/2021/08/pulling-data-into-google-sheets-from-phabricator/) about pulling in Phab issues to Google Sheets

## License
GPL 3.0 

## Project status
I have built this in my spare time to help with a specific set of metrics, this is not yet a general-purpose library. Hopefully it can be a helpful reference for specific metrics!